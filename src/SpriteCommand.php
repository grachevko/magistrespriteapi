<?php

namespace App;

use Cilex\Command\Command;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Event\Listeners\MysqlSessionInit;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * This file is part of the CLI utility to operate with www.autosprite.ru API for www.automagistre.ru
 *
 * @author Grachev Konstantin Olegovich <preemiere@ya.ru>
 */
class SpriteCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('sprite')
            ->setDescription('Send Client information to www.autosprite.ru')
            ->addArgument('operation', null, InputArgument::REQUIRED, 'Choose operation');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Connection $db */
        $db = $this->getService('db');
        $db->getEventManager()->addEventSubscriber(new MysqlSessionInit('utf8', 'utf8_unicode_ci'));
        $model = new Model($db);

        switch ($input->getArgument('operation')) {
            case 'sync':

                /** Отправка новых клиентов */
                $persons = array_merge($model->getClients(), $model->getClientsWithNewCars());

                foreach ($persons as $person) {
                    $person['cars'] = $model->getCarsByClientId($person['client_id']);
                    unset($person['client_id']);

                    $response = SpriteSender::getClientByPhone($person['phone']);
                    echo PHP_EOL . '44 response getClientByPhone' . PHP_EOL;
                    var_dump($response);
                    if (isset($response['results'][0]['id'])) {
                        $person['id'] = $response['results'][0]['id'];
                        unset($response);
                    }

                    foreach ($person['cars'] as $car) {
                        $response = SpriteSender::getCarByVin($car['vin']);
                        if (isset($response['results'][0]['id'])) {
                            $car['id'] = $response['results'][0]['id'];
                        }
                        unset($response);
                    }

                    $response = SpriteSender::postClient($person);

                    unset($person);

                    $model->setClientSpriteId($response['external_id'], $response['id']);
                    foreach ($response['cars'] as $car) {
                        $model->setCarSpriteId($car['external_id'], $car['id']);
                    }
                    unset($response);
                }

                break;
            case 'deleteAll':

                $c = 0;
                Config::getPage();

                $next = true;
                while ($next) {
                    $array = SpriteSender::getClients(false);
                    $next = isset($array['next']) && $array['next'] != 'null' ? true : false;

                    foreach ($array['results'] as $person) {
                        $c++;
                        SpriteSender::deleteClient($person['id']);
                        $output->write($c . ',');
                    }
                }

                break;
            default:
                $output->writeln(sprintf('I don\'t know this argument'));
                break;
        }
    }
}