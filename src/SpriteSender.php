<?php

namespace App;

/**
 * This file is part of the CLI utility to operate with www.autosprite.ru API for www.automagistre.ru
 *
 * @author Grachev Konstantin Olegovich <preemiere@ya.ru>
 */
class SpriteSender
{
    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';
    const METHOD_DELETE = 'DELETE';
    const METHOD_PUT = 'PUT';

    public static function postClient($data)
    {
        $response = self::send(Config::getClientUrl() . Config::URL_PARAMETERS, self::METHOD_POST, self::encodeClientData($data));
        return self::decodeClientResponse($response);
    }

    public static function getClients($next = true)
    {
        $parameter = Config::URL_PARAMETERS . '&page=' . Config::getPage($next);
        $response = self::send(Config::getClientUrl() . $parameter, self::METHOD_GET);
        return self::decodeClientResponse($response);
    }

    public static function getClientByPhone($phone) {
        if (substr($phone, 0, 1) == '8') {
            $phone = '7' . substr($phone, 1, strlen($phone));
        }
        $parameters = Config::URL_PARAMETERS . '&phone=' . $phone;
        $response = self::send(Config::getClientUrl() . $parameters, self::METHOD_GET);
        return self::decodeClientResponse($response);
    }

    public static function getCarByVin($vin) {
        $parameters = Config::URL_PARAMETERS . '&vin=' . $vin;
        $response = self::send(Config::getCarUrl() . $parameters, self::METHOD_GET);
        return json_decode($response, true);
    }

    public static function deleteClient($id)
    {
        return self::send(Config::getClientUrl() . $id . '/' . Config::URL_PARAMETERS, self::METHOD_DELETE);
    }


    private static function send($url, $method, $data_json = null)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERPWD, Config::LOGIN . ':' . Config::PASS);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Vary: Accept'));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_CAINFO, __DIR__ . "/../cacert.pem");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        if (!empty($data_json)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
        }

        $response = curl_exec($ch);

        curl_close($ch);

        return $response;
    }

    private static function encodeClientData($data)
    {
        if (isset($data['phone']) && substr($data['phone'], 0, 1) == '8') {
            $data['phone'] = '+7' . substr($data['phone'], 1, strlen($data['phone']));
        }

        $json = json_encode($data, true);

        return $json;
    }

    private static function decodeClientResponse($response)
    {
        $data = json_decode($response, true);

        return $data;
    }

//    private static function decodeReservationResponse($response) {
//        $data = json_decode($response, true);
//
//        return $data;
//    }
}
