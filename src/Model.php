<?php

namespace App;

use Doctrine\DBAL\Connection;

/**
 * This file is part of the CLI utility to operate with www.autosprite.ru API for www.automagistre.ru
 *
 * @author Grachev Konstantin Olegovich <preemiere@ya.ru>
 */
class Model
{

    /**
     * @var Connection
     */
    private $db;

    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    public function getClients()
    {
        $stmt = $this->db->prepare('SELECT p.id AS external_id, p.sprite_id AS id, c.id AS client_id, CONCAT_WS( ? , firstname, lastname) AS name, mobilephone AS phone FROM person p LEFT JOIN client c ON c.person_id = p.id WHERE LENGTH(mobilephone) = 11 AND SUBSTR(mobilephone,1,1) = 8 AND p.sprite_id IS NULL');
        $stmt->bindValue(1, ' ');
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function getClientsWithNewCars() {
        $stmt = $this->db->prepare('SELECT p.id AS external_id, p.sprite_id AS id, c.id AS client_id, CONCAT_WS( ? , firstname, lastname) AS name, mobilephone AS phone FROM person p INNER JOIN client c ON c.person_id = p.id INNER JOIN car ON car.client_id = c.id WHERE LENGTH(mobilephone) = 11 AND SUBSTR(mobilephone,1,1) = 8 AND car.sprite_id IS NULL AND car.vin IS NOT NULL AND p.sprite_id IS NOT NULL');
        $stmt->bindValue(1, ' ');
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function getCarsByClientId($id)
    {
        $stmt = $this->db->prepare('SELECT ck.name AS make, cl.name AS model, c.vin, c.gosnomer AS reg_number, c.year AS production_year, c.id AS external_id, cm.name AS version, c.sprite_id as id FROM car c LEFT JOIN carmodel cl ON c.carmodel_id = cl.id LEFT JOIN carmake ck ON c.carmake_id = ck.id LEFT JOIN carmodification cm ON c.carmodification_id = cm.id WHERE c.client_id = :id AND c.vin IS NOT NULL ');
        $stmt->bindValue('id', $id);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function setClientSpriteId($client_id, $sprite_id)
    {
        $stmt = $this->db->prepare('UPDATE person SET sprite_id = :sprite WHERE id = :id');
        $stmt->bindValue('sprite', $sprite_id);
        $stmt->bindValue('id', $client_id);
        $stmt->execute();
    }

    public function setCarSpriteId($car_id, $sprite_id)
    {
        $stmt = $this->db->prepare('UPDATE car SET sprite_id = :sprite WHERE id = :id');
        $stmt->bindValue('sprite', $sprite_id);
        $stmt->bindValue('id', $car_id);
        $stmt->execute();
    }

}