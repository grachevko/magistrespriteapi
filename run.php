<?php

/**
 * This file is part of the CLI utility to operate with www.autosprite.ru API for www.automagistre.ru
 *
 * @author Grachev Konstantin Olegovich <preemiere@ya.ru>
 */

use App\Config;

if (!$loader = include __DIR__ . '/vendor/autoload.php') {
    die('You must set up the project dependencies.');
}

$app = new \Cilex\Application('Cilex');

$app->register(new Cilex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver' => 'pdo_mysql',
        'host' => Config::DB_HOST,
        'dbname' => Config::DB_NAME,
        'user' => Config::DB_USER,
        'password' => Config::DB_PASS,
        'charset' => 'utf8'
    )
));

$app->command(new App\SpriteCommand());

function dd($value)
{
    var_dump($value);
    die;
}

$app->run();